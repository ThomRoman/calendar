const days = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado']
const months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
class Calendar {
    constructor(options){
        this.date = options.date || new Date()
        this.container = options.container
        this.onSelect = options.onSelect

        // TO DO: MANEJO DE BOTONES

        this.calendarTable = null
        this.buildTable()
        this.bindEvents()
        this.updateTable()
    }
    updateTable(){
        this.calculteDates()

        let firstDayInWeek = this.monthStart.getDay()
        let trs = [...this.calendarTable.querySelectorAll('tr')]
        for (let i = 0; i <= 5; i++) {
            let tr = trs[i]
            let tds = [...tr.querySelectorAll('td')]
            for (let j = 0; j <7; j++) {
                let td = tds[j]
                let day = (i*7) + j;
                if ( day >= firstDayInWeek && day - firstDayInWeek < this.monthLength){
                    td.innerHTML = day - firstDayInWeek + 1
                    td.style.borderStyle = 'solid'
                    td.dataset.day = day - firstDayInWeek + 1
                    td.classList.add('tr')
                }else{
                    td.style.borderStyle = 'none'
                    td.innerHTML = ''
                    td.removeAttribute('data-day')
                    td.classList.remove('tr')
                }
            }
        }
        this.container.querySelector('.header').innerHTML = `${months[this.date.getMonth()]} - ${this.date.getFullYear()}`
    }
    calculteDates(){
        this.monthStart = new Date(
            this.date.getFullYear(),
            this.date.getMonth(),
            1
        )
        this.monthEnd = new Date(
            this.date.getFullYear(),
            this.date.getMonth()+1,
            1
        )

        this.monthLength = (this.monthEnd - this.monthStart) / (1000 * 60 * 60 *24)
        this.monthLength = Math.floor(this.monthLength)
        
    }
    buildTable(){
        let table = document.createElement('table')
        let thead = document.createElement('thead')
        days.forEach(day=>{
            let td = document.createElement('td')
            td.innerHTML = day
            thead.appendChild(td)
        })
        table.appendChild(thead)
        for (let i = 0; i <= 5; i++) {
            let tr = document.createElement('tr')
            for (let j = 0; j <7; j++) {
                let td = document.createElement('td')
                tr.appendChild(td)
            }
            table.appendChild(tr)
        }
        this.calendarTable = table
        let body = document.createElement('div')
        table.addEventListener('click',event=>{
            const target = event.target
            if(target.tagName!=='TD')
                return
            if(target.getAttribute('data-day')){
                const selectDay = new Date(this.date.getFullYear(),this.date.getMonth(),target.dataset.day)
                this.onSelect(selectDay)
            }
            
            
        })
        body.classList.add('body')
        body.appendChild(table)
        let header = document.createElement('div')
        header.classList.add('header')
        this.container.appendChild(header)
        this.container.appendChild(body)
    }

    next(){
        let month = this.date.getMonth()
        if(month === 11) {
            this.date = new Date(this.date.getFullYear()+1,0,1)
        }else{
            this.date = new Date(this.date.getFullYear(),month+1,1)
        }
        this.updateTable()
    }
    prev(){
        let month = this.date.getMonth()
        if(month === 0) {
            this.date = new Date(this.date.getFullYear()-1,11,1)
        }else{
            this.date = new Date(this.date.getFullYear(),month -1,1)
        }
        
        this.updateTable()
    }
    bindEvents(){
        this.buttons = {}
        this.buttons.prev = document.createElement('button')
        this.buttons.next = document.createElement('button')

        this.buttons.prev.innerHTML = 'Anteriror'
        this.buttons.prev.classList.add('prev')
        this.buttons.next.innerHTML = 'Siguiente'
        this.buttons.next.classList.add('next')
        const actions = document.createElement('div')
        actions.classList.add('actions')
        actions.append(...Object.values(this.buttons))
        this.container.querySelector('.body').append(actions)
        actions.addEventListener('click',event=>{
            const target = event.target
            if(target.tagName !== 'BUTTON')
                return
            if(target.classList.contains('prev')){
                this.prev()
            }
            if(target.classList.contains('next')){
                this.next()
            }
        })
    }
}

const calendar = new Calendar({
    container : document.querySelector('.calendar'),
    onSelect : function (date){
        console.log("date", date)
    }
})